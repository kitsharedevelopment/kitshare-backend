<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manufacturers extends CI_Controller {
	 public function __construct() {
		parent::__construct();
		$this->load->helper(array('url','form','html','text'));
		$this->load->library(array('session','form_validation','pagination','email'));
		$this->load->model(array('common_model','mail_model'));
		if($this->session->userdata('ADMIN_ID') =='') {
		  redirect('login');
		}
	}
	
	protected $validation_rules = array
        (
		'Add' => array(
            array(
                'field' => 'manufacturer',
                'label' => 'manufacturer',
                'rules' => 'trim|required'
            )
        ),
    );

	
	public function index()
	{
		$data=array();
		$where = " ";
		$limit=10;
		
		$data['manufacturer']				= $this->input->get('manufacturer');
		if($data['manufacturer'] != ''){
				$where .=  " where manufacturer_name LIKE '%".trim($data['manufacturer'])."%'";
			}
		
		if($this->input->get("per_page")!= '')
		{
		$offset = $this->input->get("per_page");
		}
		else
		{
			$offset=0;
		}
		$nSer="SELECT * FROM gs_manufacturers ".$where." ORDER BY manufacturer_id DESC";
		$sql=$nSer." LIMIT ".$limit." OFFSET  ".$offset." ";
		$result=$this->db->query($sql);		
		$total_rows=count($this->db->query($nSer)->result());	
		$data['result'] = $result;
		$data['total_rows']=$total_rows;
		$data['limit']=$limit;
		$config['base_url'] = base_url()."manufacturer/?manufacturer=".$data['manufacturer']."";
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $limit;
		$config['page_query_string'] = TRUE;
	    $config['full_tag_open'] = "<ul class='pagination pagination-sm text-center'>";
		$config['full_tag_close'] = "</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$paginator = $this->pagination->create_links();
//////////////////////////////Pagination config//////////////////////////////////				
		
		$data['paginator'] = $paginator;
		$this->load->view('common/header');	
		$this->load->view('common/left-menu');	
		$this->load->view('manufacturer/list', $data);
		$this->load->view('common/footer');		
	}
	public function add()
	{
		$data=array();
		$this->load->view('common/header');	
		$this->load->view('common/left-menu');	
		$this->load->view('manufacturer/add', $data);
		$this->load->view('common/footer');	
	}
	public function save()
	{
	$data=array();
	
	
	$this->form_validation->set_rules($this->validation_rules['Add']);
	
	
	
	if($this->form_validation->run())
	{

	
		$data['manufacturer_name']= $this->input->post('manufacturer');
		
		
		$data['is_active']= $this->input->post('status');	
		$data['create_date']= date('Y-m-d') ;
		$data['create_user'] = $this->session->userdata('ADMIN_ID');
		
		$this->common_model->addRecord('gs_manufacturers',$data);
		$message = '<div class="callout callout-success">manufacturer has been successfully added.</p></div>';
		$this->session->set_flashdata('success', $message);
		redirect('manufacturers');
					
	}
	else
	{
		$this->load->view('common/header');	
		$this->load->view('common/left-menu');					
		$this->load->view('manufacturer/add', $data);
		$this->load->view('common/footer');	
	}
	
	}
	public function edit()
	{
	    $data = array();
		$id = $this->uri->segment(3);
		$where_array = array('manufacturer_id'=>$id);
		$data['result']= $this->common_model->get_all_record('gs_manufacturers',$where_array);	
		$this->load->view('common/header');	
		$this->load->view('common/left-menu');					
		$this->load->view('manufacturer/edit', $data);
		$this->load->view('common/footer');		
	}
	public function update()
	{
		$data = array();
    	$id = $this->input->post('manufacturer_id');
   
		$this->form_validation->set_rules($this->validation_rules['Add']);

		if($this->form_validation->run() == true )
		{
	
		$row['manufacturer_name']= $this->input->post('manufacturer');
		$row['is_active']= $this->input->post('status');	
		$row['update_date']= date('Y-m-d') ;
		$row['update_user'] = $this->session->userdata('ADMIN_ID');
		
		
		$this->db->where('manufacturer_id', $id);
		$this->db->update('gs_manufacturers', $row); 
		
		
		
		$message = '<div class="callout callout-success"><p>manufacturer updated successfully.</p></div>';
		$this->session->set_flashdata('success', $message);
		redirect('manufacturers');
	
		}
		else
		{	
			$this->load->view('common/header');	
			$this->load->view('common/left-menu');
			$this->load->view('manufacturer/edit', $data);
			$this->load->view('common/footer');	
		}
	
	}
	public function select_delete()
	{
	if(isset($_POST['bulk_delete_submit']))
	{
    $idArr = $this->input->post('checked_id');
    foreach($idArr as $id){
	        
			$where_array = array('manufacturer_id'=>$id);
			$lang= $this->common_model->get_all_record('gs_manufacturers',$where_array);
			
			foreach($lang as $im){
				$this->load->helper("file");
				$oldfile = manufacturer."/".$im->manufacturer_logo;
				unlink($oldfile);
			}
			
     		$this->common_model->delele('gs_manufacturers','manufacturer_id',$id);
			
    	}
		$message = '<div class="callout callout-success"><p>manufacturers have been deleted successfully.</p></div>';
		$this->session->set_flashdata('success', $message);
		redirect('manufacturers');
	 }
	}
	public function delete_record()
	{
		$id=$this->uri->segment(3);
		$where_array = array('manufacturer_id'=>$id);
		$manufacturer= $this->common_model->get_all_record('gs_manufacturers',$where_array);
		foreach($manufacturer as $im){
			$this->load->helper("file");
			$oldfile = manufacturer."/".$im->manufacturer_logo;
			unlink($oldfile);
		}
		
		$this->common_model->delele('gs_manufacturers','manufacturer_id',$id);
		
		$message = '<div class="callout callout-success"><p>manufacturers have been deleted successfully.</p></div>';
		$this->session->set_flashdata('success', $message);
		redirect('manufacturers');
	}
	
	
}?>