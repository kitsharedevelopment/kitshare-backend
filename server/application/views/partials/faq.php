<section class="faq">
<div class="container">
<div class="col-md-12">
<div class="logo3 animated bounceInDown">
          <img class="img-responsive" src="assets/images/logo.png">
          </div>
<h1>Advice and answers from the KitShare Team</h1>
</div>
<div class="row">
           <div id="custom-search-input">
                <div class="input-group col-md-12">
				
				<form role="search" class="center">
        <div class="input-group">
            <input type="text" name="q" placeholder="Search for answers..." class="form-control search__input">
            <div class="input-group-btn">
                <button type="submit" class="btn btn-default search__input1"><i class="glyphicon glyphicon-search"></i></button>
            </div>
        </div>
        </form>
				
                </div>
            </div>
	</div>
</div>




</section>

<section class="faq_sec">
<div class="container">
      <div class="content educate_content"><section class="section">
    <div class="g__space">
      <a class="paper " href="#">
        <div class="collection o__ltr">
          <div class="collection__photo">
            <svg viewBox="0 0 48 48" role="img"><g fill-rule="evenodd" fill="none" id="chat-question"><path stroke-linejoin="round" stroke-width="2" d="M47 21.268c0 10.363-10.297 18.765-23 18.765-2.835 0-5.55-.418-8.058-1.184L2.725 45 7.9 34.668c-4.258-3.406-6.9-8.15-6.9-13.4C1 10.904 11.297 2.502 24 2.502s23 8.402 23 18.766z"/><path fill="#231F1F" d="M25 28.502a2 2 0 1 0 0 4 2 2 0 0 0 0-4"/><path stroke-linejoin="round" stroke-linecap="round" stroke-width="2" d="M19 17.75c0-3.312 2.686-6.124 6-6.124 3.313 0 6 2.626 6 5.938 0 3.315-2.687 5.938-6 5.938V26"/></g></svg>
          </div>
          <div dir="ltr" class="collection_meta">
            <h2 class="t__h3 c__primary">Getting Started</h2>
            <p class="paper__preview">What is ShareGrid, where is it available, and how do I sign up for a profile?</p>
            <div class="avatar">
  <div class="avatar__photo avatars__images o__ltr">
        <img class="avatar__image" src="https://static.intercomassets.com/avatars/985130/square_128/fullsizeoutput_4-1485974929.jpeg?1485974929">

  </div>
  <div class="avatar__info">
    <div>
      <span class="c__darker">
        9 articles in this collection
      </span>
      <br>
      Written by <span class="c__darker"> Harry Werber</span>
    </div>
  </div>
</div>

          </div>
        </div>
      </a>
    </div>
    <div class="g__space">
      <a class="paper " href="#">
        <div class="collection o__ltr">
          <div class="collection__photo">
            <svg viewBox="0 0 48 48" role="img"><g fill-rule="evenodd" fill="none" id="user-profile"><path stroke-linejoin="round" stroke-width="2" d="M29 12h18v34H1V12h18"/><path stroke-width="2" d="M18 30a4 4 0 1 1-8 0 4 4 0 0 1 8 0z"/><path stroke-width="2" d="M14 34c-5 0-7 3-7 6h14c0-3-2-6-7-6z"/><path stroke-linejoin="round" stroke-width="2" d="M29 16H19V8c0-2.75 2.25-5 5-5s5 2.25 5 5v8z"/><path fill="#231F1F" d="M24 6a2 2 0 1 0 0 4 2 2 0 0 0 0-4"/><path stroke-width="2" d="M1 20h46M26 38h16m-16-4h16m-16-4h16m-16-4h8"/></g></svg>
          </div>
          <div dir="ltr" class="collection_meta">
            <h2 class="t__h3 c__primary">Identification &amp; Verifications</h2>
            <p class="paper__preview">How and why we verify each user's ID, why we encourage members to provide as much info as possible, and how we protect your information.</p>
            <div class="avatar">
  <div class="avatar__photo avatars__images o__ltr">
        <img class="avatar__image" src="https://static.intercomassets.com/avatars/985130/square_128/fullsizeoutput_4-1485974929.jpeg?1485974929">

  </div>
  <div class="avatar__info">
    <div>
      <span class="c__darker">
        15 articles in this collection
      </span>
      <br>
      Written by <span class="c__darker"> Harry Werber</span>
    </div>
  </div>
</div>

          </div>
        </div>
      </a>
    </div>
    <div class="g__space">
      <a class="paper " href="#">
        <div class="collection o__ltr">
          <div class="collection__photo">
            <svg viewBox="0 0 48 48" role="img"><g fill-rule="evenodd" fill="none" stroke-width="2" id="user-edit"><path stroke-linejoin="round" stroke-linecap="round" d="M34 45l-7 2 2-7 13-13 5 5-13 13z"/><path stroke-linejoin="round" d="M38 31l5 5"/><path stroke-linejoin="round" stroke-linecap="round" d="M29 40l5 5"/><path d="M31 31.037L25 29v-5.843m-10 .373V29L4.98 32.577A6 6 0 0 0 1 38.227V43h21"/><path d="M29.75 13c0 6.627-4.48 12-10 12-5.523 0-10-5.373-10-12s4.477-12 10-12c5.52 0 10 5.373 10 12z"/><path d="M29.667 11.916c-.336.04-.58.018-.917.083-3.406.653-5.593-.58-7.468-3.86C20.157 10.293 16.64 12 13.75 12c-1.422 0-2.646-.292-3.87-.933"/></g></svg>
          </div>
          <div dir="ltr" class="collection_meta">
            <h2 class="t__h3 c__primary">Account &amp; Profile</h2>
            <p class="paper__preview"></p>
            <div class="avatar">
  <div class="avatar__photo avatars__images o__ltr">
        <img class="avatar__image" src="https://static.intercomassets.com/avatars/985130/square_128/fullsizeoutput_4-1485974929.jpeg?1485974929">

  </div>
  <div class="avatar__info">
    <div>
      <span class="c__darker">
        5 articles in this collection
      </span>
      <br>
      Written by <span class="c__darker"> Harry Werber</span>
    </div>
  </div>
</div>

          </div>
        </div>
      </a>
    </div>
    <div class="g__space">
      <a class="paper " href="#">
        <div class="collection o__ltr">
          <div class="collection__photo">
            <svg viewBox="0 0 48 48" role="img"><g fill-rule="evenodd" fill="none" stroke-width="2" id="comms-video"><path stroke-linejoin="round" stroke-linecap="round" d="M29 33H11V15h18v7l8-5v14l-8-5v7z"/><path d="M47 24c0 12.703-10.297 23-23 23C11.298 47 1 36.704 1 24 1 11.3 11.298 1 24 1c12.703 0 23 10.3 23 23z"/></g></svg>
          </div>
          <div dir="ltr" class="collection_meta">
            <h2 class="t__h3 c__primary">Renting Gear</h2>
            <p class="paper__preview"></p>
            <div class="avatar">
  <div class="avatar__photo avatars__images o__ltr">
        <img class="avatar__image" src="https://static.intercomassets.com/avatars/985130/square_128/fullsizeoutput_4-1485974929.jpeg?1485974929">

        <img class="avatar__image" src="https://static.intercomassets.com/avatars/46735/square_128/Picture_5-1462404500-1462456672.png?1462456672">

  </div>
  <div class="avatar__info">
    <div>
      <span class="c__darker">
        13 articles in this collection
      </span>
      <br>
      Written by <span class="c__darker"> Harry Werber</span> and <span class="c__darker"> Arash Shiva</span>
    </div>
  </div>
</div>

          </div>
        </div>
      </a>
    </div>
    <div class="g__space">
      <a class="paper " href="#">
        <div class="collection o__ltr">
          <div class="collection__photo">
            <svg viewBox="0 0 48 48" role="img"><g fill-rule="evenodd" fill="none" id="biz-shop"><path stroke-linejoin="round" stroke-linecap="round" stroke-width="2" d="M43 19.625V44H5V19.625"/><path stroke-linejoin="round" stroke-linecap="round" stroke-width="2" d="M25 38H9V26h16v12zm14 6H29V26h10v18zm4-40H5l-4 8h46l-4-8zm4 10a6 6 0 0 1-6 6c-1.652 0-2.914-.92-4-2-1.416 2.25-4.146 4-7 4a7.973 7.973 0 0 1-6-2.72A7.97 7.97 0 0 1 18 22c-2.854 0-5.584-1.75-7-4-1.084 1.08-2.348 2-4 2a6 6 0 0 1-6-6v-2h46v2z"/><path stroke-linejoin="round" stroke-linecap="round" stroke-width="2" d="M11 18v-6l2-8m11 15.28V4m13 14v-6l-2-8"/><path fill="#231F1F" d="M35 35a1 1 0 1 0 0 2 1 1 0 0 0 0-2"/></g></svg>
          </div>
          <div dir="ltr" class="collection_meta">
            <h2 class="t__h3 c__primary">Listing Gear &amp; Managing Rentals</h2>
            <p class="paper__preview"></p>
            <div class="avatar">
  <div class="avatar__photo avatars__images o__ltr">
        <img class="avatar__image" src="https://static.intercomassets.com/avatars/985130/square_128/fullsizeoutput_4-1485974929.jpeg?1485974929">

        <img class="avatar__image" src="https://static.intercomassets.com/avatars/777230/square_128/photo-1486744147.png?1486744147">

  </div>
  <div class="avatar__info">
    <div>
      <span class="c__darker">
        19 articles in this collection
      </span>
      <br>
      Written by <span class="c__darker"> Harry Werber</span> and <span class="c__darker"> James Keblas</span>
    </div>
  </div>
</div>

          </div>
        </div>
      </a>
    </div>
    <div class="g__space">
      <a class="paper " href="#">
        <div class="collection o__ltr">
          <div class="collection__photo">
            <svg viewBox="0 0 48 48" role="img"><g stroke-linejoin="round" fill-rule="evenodd" fill="none" stroke-width="2" id="biz-dollar"><path d="M47 24c0 12.703-10.297 23-23 23C11.3 47 1 36.703 1 24 1 11.298 11.3 1 24 1c12.703 0 23 10.298 23 23z"/><path stroke-linecap="round" d="M19 29a5 5 0 0 0 5 5 5 5 0 0 0 0-10 5 5 0 0 1 0-10 5 5 0 0 1 5 5m-5-8v26"/></g></svg>
          </div>
          <div dir="ltr" class="collection_meta">
            <h2 class="t__h3 c__primary">Payments &amp; Pricing</h2>
            <p class="paper__preview"></p>
            <div class="avatar">
  <div class="avatar__photo avatars__images o__ltr">
        <img class="avatar__image" src="https://static.intercomassets.com/avatars/985130/square_128/fullsizeoutput_4-1485974929.jpeg?1485974929">

        <img class="avatar__image" src="https://static.intercomassets.com/avatars/38628/square_128/Brent_Alexa_Mini-1514942225.jpeg?1514942225">

        <img class="avatar__image" src="https://static.intercomassets.com/avatars/46735/square_128/Picture_5-1462404500-1462456672.png?1462456672">

  </div>
  <div class="avatar__info">
    <div>
      <span class="c__darker">
        16 articles in this collection
      </span>
      <br>
      Written by <span class="c__darker"> Harry Werber,</span> <span class="c__darker"> Brent Barbano,</span> and <span class="c__darker"> Arash Shiva</span>
    </div>
  </div>
</div>

          </div>
        </div>
      </a>
    </div>
    <div class="g__space">
      <a class="paper " href="#">
        <div class="collection o__ltr">
          <div class="collection__photo">
            <svg viewBox="0 0 48 48" role="img"><g stroke-linecap="round" fill-rule="evenodd" fill="none" stroke-width="2" id="book-bookmark"><path d="M35 31l-6-6-6 6V7h12v24z"/><path stroke-linejoin="round" d="M35 9h6v38H11a4 4 0 0 1-4-4V5"/><path stroke-linejoin="round" d="M39 9V1H11a4 4 0 0 0 0 8h12"/></g></svg>
          </div>
          <div dir="ltr" class="collection_meta">
            <h2 class="t__h3 c__primary">Insurance &amp; Coverage Options</h2>
            <p class="paper__preview"></p>
            <div class="avatar">
  <div class="avatar__photo avatars__images o__ltr">
        <img class="avatar__image" src="https://static.intercomassets.com/avatars/985130/square_128/fullsizeoutput_4-1485974929.jpeg?1485974929">

  </div>
  <div class="avatar__info">
    <div>
      <span class="c__darker">
        16 articles in this collection
      </span>
      <br>
      Written by <span class="c__darker"> Harry Werber</span>
    </div>
  </div>
</div>

          </div>
        </div>
      </a>
    </div>
    <div class="g__space">
      <a class="paper " href="#">
        <div class="collection o__ltr">
          <div class="collection__photo">
            <svg viewBox="0 0 48 48" role="img"><g stroke-linejoin="round" stroke-linecap="round" fill-rule="evenodd" fill="none" stroke-width="2" id="book-star"><path d="M11 5h24v4"/><path d="M39 9V1H11a4 4 0 0 0 0 8h30v38H11a4 4 0 0 1-4-4V5"/><path d="M24 15.975L27 25h9l-7 5 3 9-8-5-8 5 3-9-7-5h9l3-9.025z"/></g></svg>
          </div>
          <div dir="ltr" class="collection_meta">
            <h2 class="t__h3 c__primary">Reviews</h2>
            <p class="paper__preview"></p>
            <div class="avatar">
  <div class="avatar__photo avatars__images o__ltr">
        <img class="avatar__image" src="https://static.intercomassets.com/avatars/985130/square_128/fullsizeoutput_4-1485974929.jpeg?1485974929">

  </div>
  <div class="avatar__info">
    <div>
      <span class="c__darker">
        2 articles in this collection
      </span>
      <br>
      Written by <span class="c__darker"> Harry Werber</span>
    </div>
  </div>
</div>

          </div>
        </div>
      </a>
    </div>
    <div class="g__space">
      <a class="paper " href="#">
        <div class="collection o__ltr">
          <div class="collection__photo">
            <svg viewBox="0 0 48 48" role="img"><g id="info-cross"><path stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2" d="M46 24.043c-.023 12.15-9.893 21.98-22.042 21.958C11.806 45.98 1.978 36.11 2 23.96 2.023 11.808 11.89 1.978 24.042 2c12.15.024 21.98 9.892 21.958 22.043zm-13.5-8.51l-17 16.937m16.968.033L15.53 15.5"/></g></svg>
          </div>
          <div dir="ltr" class="collection_meta">
            <h2 class="t__h3 c__primary">Cancellations &amp; Refunds</h2>
            <p class="paper__preview"></p>
            <div class="avatar">
  <div class="avatar__photo avatars__images o__ltr">
        <img class="avatar__image" src="https://static.intercomassets.com/avatars/985130/square_128/fullsizeoutput_4-1485974929.jpeg?1485974929">

  </div>
  <div class="avatar__info">
    <div>
      <span class="c__darker">
        2 articles in this collection
      </span>
      <br>
      Written by <span class="c__darker"> Harry Werber</span>
    </div>
  </div>
</div>

          </div>
        </div>
      </a>
    </div>
    <div class="g__space">
      <a class="paper " href="#">
        <div class="collection o__ltr">
          <div class="collection__photo">
            <svg viewBox="0 0 48 48" role="img"><g stroke-linejoin="round" stroke-linecap="round" fill-rule="evenodd" fill="none" stroke-width="2" id="info-check"><path d="M34.002 17l-15 14-5-5"/><path d="M47.002 24c0 12.703-10.3 23-23 23-12.706 0-23-10.297-23-23s10.294-23 23-23c12.7 0 23 10.297 23 23z"/></g></svg>
          </div>
          <div dir="ltr" class="collection_meta">
            <h2 class="t__h3 c__primary">Damage &amp; Claims</h2>
            <p class="paper__preview"></p>
            <div class="avatar">
  <div class="avatar__photo avatars__images o__ltr">
       

        <img class="avatar__image" src="https://static.intercomassets.com/avatars/985130/square_128/fullsizeoutput_4-1485974929.jpeg?1485974929">

  </div>
  <div class="avatar__info">
    <div>
      <span class="c__darker">
        7 articles in this collection
      </span>
      <br>
      Written by <span class="c__darker"> ShareGrid</span> and <span class="c__darker"> Harry Werber</span>
    </div>
  </div>
</div>

          </div>
        </div>
      </a>
    </div>
</section>
</div>
    </div>
</section>